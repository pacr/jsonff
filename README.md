jsonff
=====

[![pipeline status](https://gitlab.com/everythingfunctional/jsonff/badges/main/pipeline.svg)](https://gitlab.com/everythingfunctional/jsonff/commits/main)

# This Project is Now Dormant!

It has been superseded by the new project [rojff](https://gitlab.com/everythingfunctional/rojff)

This library proved to be unusable for large data sets.
It was unable to construct the string for large outputs due to such large strings being produced and copied around.
Additionally, the parser was incredibly slow, primarily due to the massive amounts of copies being created as the data structures were built up.
The new library solves these problems, but with a similar API.
However, since there were some breaking changes, I decided to implement it from scratch as a new project.

JSON for Fortran.

The constants `JSON_NULL`, `JSON_TRUE` and `JSON_FALSE` and the constructors
`json_number_t`, `fallible_json_string_t`, `json_array_t`, `json_member_t` and `json_object_t` are
provided for building up JSON data structures in Fortran. Note that the procedure
to create a `json_string_t` ensures that the string is valid according to the JSON
standard. *Unsafe* versions of the json string, member, and object
procedures are provided in the event you are certain that you are only dealing
with valid strings.

Once constructed, JSON values can be converted to string representation in
either compact or expanded, human-readable formats. Additionally, procedures are
provided (`parse_json`) that parse a string into a JSON data structure. It also
provides reasonable error messages in the event the string does not contain
valid JSON.

A string generated from a valid JSON data structure is guaranteed to be able to
be parsed by the parser into exactly the same data structure. Note that a data
structure parsed from a string is not necessarily guaranteed to produce exactly
the same string, since formatting is not important to JSON data.


Using jsonff
------------

This section provides some examples to use jsonff in your project.
A detailed references on the functionality of jsonff is available in
[the developer documentation](https://everythingfunctional.gitlab.io/jsonff).


### Reading and writing a JSON

To create a simple reader and writer for JSON three steps are usually required.
A small and concise example to read JSON from a string and pretty print it is given here.

```Fortran
program example
    use iso_varying_string
    use jsonff
    use erloff
    implicit none
    class(json_value_t), allocatable :: json
    type(fallible_json_value_t) :: parsed_json
    type(error_list_t) :: errors
    type(varying_string) :: string

    parsed_json = parse_json_from_file('index.json')

    if (parsed_json%failed()) then
        errors = parsed_json%errors()
        call put_line(errors%to_string())
        error stop
    end if

    json = parsed_json%value_()

    string = json%to_expanded_string()
end program example
```

A JSON document can be parsed from a string with the ``parse_json`` function or
by reading a file with ``parse_json_from_file`` function.
Both functions return an instance of a ``fallible_json_value_t`` type representing
a union of an ``error_list_t`` and a ``json_value_t``.
To verify the correctness of the parsed JSON the ``fallible_json_value_t`` can be
checked with the ``%failed()`` method. In case of failure the ``error_list_t``
should be retrieved with the ``%errors()`` method and handled appropriately by
the caller.

If the JSON document was correct the ``json_value_t`` can be obtained by
the ``%value_()`` method and processed further.
Serialization with the ``%to_expanded_string()`` or ``%to_compact_string()`` method
returns a ``varying_string`` instance.

The *literal* values (e.g. null, true and false) are identified by their type alone and contain no data.
The scalar values (e.g. string and number) contain their values, which can be accessed via `get_value` type bound procedures.
The composite values (e.g. object and array), contain zero or more elements, which can be accessed via the `get_element` type bound procedures.
Retrieving an element from an object requires a string (the key), and from an array requires an integer (the index).
Both procedures return a `fallible_json_value_t`, as it would be possible to ask for an element that does not exist.
Additionally, one can get the whole array of values from a json array as an array of `json_element_t` objects via `get_elements`.
One can get arrays of keys (as `varying_string`s) and values (as `json_element_t`s) from an object, which are guaranteed to be returned in matching order, via `get_keys` and `get_values`.
