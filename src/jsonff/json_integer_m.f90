module jsonff_json_integer_m
    use iso_varying_string, only: varying_string
    use jsonff_json_value_m, only: json_value_t
    use strff, only: to_string

    implicit none
    private
    public :: json_integer_t

    type, extends(json_value_t) :: json_integer_t
        private
        integer :: number
    contains
        private
        procedure, public :: get_value
        procedure, public :: to_compact_string => integer_to_string
        procedure, public :: to_expanded_string => integer_to_string
        procedure, public :: to_lines
    end type

    interface json_integer_t
        module procedure constructor
    end interface
contains
    elemental function constructor(number) result(json_integer)
        integer, intent(in) :: number
        type(json_integer_t) :: json_integer

        json_integer%number = number
    end function

    elemental function get_value(self) result(number)
        class(json_integer_t), intent(in) :: self
        integer :: number

        number = self%number
    end function

    pure function integer_to_string(self) result(string)
        class(json_integer_t), intent(in) :: self
        type(varying_string) :: string

        string = to_string(self%number)
    end function

    pure function to_lines(self) result(lines)
        class(json_integer_t), intent(in) :: self
        type(varying_string), allocatable :: lines(:)

        lines = [self%to_compact_string()]
    end function
end module
