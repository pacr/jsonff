module json_test
    use erloff, only: error_list_t, NOT_FOUND, OUT_OF_BOUNDS
    use iso_varying_string, only: varying_string, operator(//)
    use jsonff, only: &
            fallible_json_string_t, &
            fallible_json_value_t, &
            json_array_t, &
            json_element_t, &
            json_integer_t, &
            json_number_t, &
            json_object_t, &
            json_string_t, &
            json_member_unsafe, &
            json_string_unsafe, &
            INVALID_INPUT, &
            JSON_FALSE, &
            JSON_NULL, &
            JSON_TRUE
    use strff, only: to_string, NEWLINE
    use vegetables, only: &
            result_t, &
            test_item_t, &
            assert_equals, &
            assert_includes, &
            assert_not, &
            assert_that, &
            describe, &
            fail, &
            it

    implicit none
    private
    public :: test_json
contains
    function test_json() result(tests)
        type(test_item_t) :: tests

        tests = describe( &
                "JSON", &
                [ it( &
                        "null has the correct string representation", &
                        check_null_to_string) &
                , it( &
                        "true has the correct string representation", &
                        check_true_to_string) &
                , it( &
                        "false has the correct string representation", &
                        check_false_to_string) &
                , it("a string can be converted back", check_string_to_string) &
                , it( &
                        "a number has the correct string representation", &
                        check_number_to_string) &
                , it( &
                        "a number gets converted to a string with the specified precision", &
                        check_number_with_precision) &
                , it( &
                        "an integer has the correct string representation", &
                        check_integer_to_string) &
                , it( &
                        "an array has the correct string representation", &
                        check_array_to_string) &
                , it( &
                        "an object has the correct string representation", &
                        check_object_to_string) &
                , it( &
                        "a complex object has the correct string representation", &
                        check_complex_object_to_string) &
                , it( &
                        "can be generated in an expaned, easier to read form", &
                        check_pretty_printing) &
                , it("can extract a value from an array", get_value_from_array) &
                , it( &
                        "the elements of an array match getting them one at a time", &
                        get_array_elements) &
                , it( &
                        "extracting a value from an array with a position greater" &
                        // " than the number of elements in the array is an error", &
                        get_value_from_array_failure) &
                , it("can extract a value from an object", get_value_from_object) &
                , it( &
                        "the keys and values of an object a returned in a consistent order", &
                        get_keys_and_values) &
                , it( &
                        "extracting a value from an object with a key it doesn't have is an error", &
                        get_value_from_object_failure) &
                , it( &
                        "trying to create an invalid string is an error", &
                        check_string_error) &
                ])
    end function

    pure function check_null_to_string() result(result_)
        type(result_t) :: result_

        result_ = assert_equals("null", JSON_NULL%to_compact_string())
    end function

    pure function check_true_to_string() result(result_)
        type(result_t) :: result_

        result_ = assert_equals("true", JSON_TRUE%to_compact_string())
    end function

    pure function check_false_to_string() result(result_)
        type(result_t) :: result_

        result_ = assert_equals("false", JSON_FALSE%to_compact_string())
    end function

    pure function check_string_to_string() result(result_)
        type(result_t) :: result_

        type(json_string_t) :: string

        string = json_string_unsafe("Hello")

        result_ = assert_equals('"Hello"', string%to_compact_string())
    end function

    pure function check_number_to_string() result(result_)
        type(result_t) :: result_

        type(json_number_t) :: number

        number = json_number_t(1.0d0)

        result_ = assert_equals("1.0", number%to_compact_string())
    end function

    pure function check_number_with_precision() result(result_)
        type(result_t) :: result_

        type(json_number_t) :: number

        number = json_number_t(1.234d0, 3)

        result_ = assert_equals("1.23", number%to_compact_string())
    end function

    pure function check_integer_to_string() result(result_)
        type(result_t) :: result_

        type(json_integer_t) :: number

        number = json_integer_t(1)

        result_ = assert_equals("1", number%to_compact_string())
    end function

    function check_array_to_string() result(result_)
        type(result_t) :: result_

        type(json_array_t) :: array

        call array%append(JSON_NULL)
        call array%append(json_string_unsafe("Hello"))
        call array%append(json_number_t(2.0d0))
        result_ = assert_equals('[null,"Hello",2.0]', array%to_compact_string())
    end function

    function check_object_to_string() result(result_)
        type(result_t) :: result_

        type(json_object_t) :: object
        type(varying_string) :: string

        call object%add_unsafe("sayHello", JSON_TRUE)
        call object%add_unsafe("aNumber", json_number_t(3.0d0))
        string = object%to_compact_string()

        result_ = &
                assert_includes('"sayHello":true', string) &
                .and.assert_includes('"aNumber":3.0', string) &
                .and.assert_includes("{", string) &
                .and.assert_includes("}", string) &
                .and.assert_includes(",", string)
    end function

    function check_complex_object_to_string() result(result_)
        type(result_t) :: result_

        character(len=*), parameter :: EXPECTED = &
                '{"Hello":[null,{"World":1.0},true]}'
        type(json_array_t) :: array
        type(json_object_t) :: inner_object
        type(json_object_t) :: outer_object

        call inner_object%add_unsafe("World", json_number_t(1.0d0))
        call array%append(JSON_NULL)
        call array%append(inner_object)
        call array%append(JSON_TRUE)
        call outer_object%add_unsafe("Hello", array)

        result_ = assert_equals(EXPECTED, outer_object%to_compact_string())
    end function

    function check_pretty_printing() result(result_)
        type(result_t) :: result_

        character(len=*), parameter :: EXPECTED = &
   '{' // NEWLINE &
// '    "Hello" : [' // NEWLINE &
// '        null,' // NEWLINE &
// '        {' // NEWLINE &
// '            "World" : 1.0' // NEWLINE &
// '        },' // NEWLINE &
// '        true' // NEWLINE &
// '    ]' // NEWLINE &
// '}'
        type(json_array_t) :: array
        type(json_object_t) :: inner_object
        type(json_object_t) :: outer_object

        call inner_object%add_unsafe("World", json_number_t(1.0d0))
        call array%append(JSON_NULL)
        call array%append(inner_object)
        call array%append(JSON_TRUE)
        call outer_object%add_unsafe("Hello", array)

        result_ = assert_equals(EXPECTED, outer_object%to_expanded_string())
    end function

    function get_value_from_array() result(result_)
        type(result_t) :: result_

        type(json_array_t) :: array
        type(error_list_t) :: errors
        type(fallible_json_value_t) :: retrieved

        call array%append(json_string_unsafe("first"))
        call array%append(json_string_unsafe("second"))
        call array%append(json_string_unsafe("third"))

        retrieved = array%get_element(3)
        errors = retrieved%errors()

        result_ = assert_not(errors%has_any(), errors%to_string())
        if (result_%passed()) then
            associate(retrieved_ => retrieved%value_())
                result_ = assert_equals( &
                        '"third"', retrieved_%to_compact_string())
            end associate
        end if
    end function

    function get_array_elements() result(result_)
        type(result_t) :: result_

        type(json_array_t) :: array
        type(json_element_t), allocatable :: elements(:)
        type(error_list_t) :: errors
        integer :: i
        type(fallible_json_value_t) :: maybe_item

        array = json_array_t(json_element_t( &
                [ json_string_unsafe("first") &
                , json_string_unsafe("second") &
                , json_string_unsafe("third") &
                ]))
        elements = array%get_elements()

        result_ = assert_equals(array%length(), size(elements))
        do i = 1, array%length()
            maybe_item = array%get_element(i)
            if (maybe_item%failed()) then
                errors = maybe_item%errors()
                result_ = result_.and.fail(errors%to_string())
            else
                select type (item_string => maybe_item%value_())
                type is (json_string_t)
                    select type (element_string => elements(i)%value_())
                    type is (json_string_t)
                        result_ = result_.and.assert_equals(item_string%get_value(), element_string%get_value())
                    class default
                        result_ = result_.and.fail("didn't get a string from element " // to_string(i))
                    end select
                class default
                    result_ = result_.and.fail("didn't get a string from item " // to_string(i))
                end select
            end if
        end do
    end function

    function get_value_from_array_failure() result(result_)
        type(result_t) :: result_

        type(json_array_t) :: array
        type(error_list_t) :: errors
        type(fallible_json_value_t) :: retrieved

        call array%append(json_string_unsafe("first"))
        call array%append(json_string_unsafe("second"))
        call array%append(json_string_unsafe("third"))

        retrieved = array%get_element(4)
        errors = retrieved%errors()

        result_ = assert_that( &
                errors.hasType.OUT_OF_BOUNDS, errors%to_string())
    end function

    function get_value_from_object() result(result_)
        type(result_t) :: result_

        type(error_list_t) :: errors
        type(json_object_t) :: object
        type(fallible_json_value_t) :: retrieved

        call object%add_unsafe("first", json_string_unsafe("hello"))
        call object%add_unsafe("second", json_string_unsafe("goodbye"))

        retrieved = object%get_element("first")
        errors = retrieved%errors()

        result_ = assert_not(errors%has_any(), errors%to_string())
        if (result_%passed()) then
            associate(retrieved_ => retrieved%value_())
                result_ = assert_equals( &
                        '"hello"', retrieved_%to_compact_string())
            end associate
        end if
    end function

    function get_keys_and_values() result(result_)
        type(result_t) :: result_

        type(error_list_t) :: errors
        type(varying_string), allocatable :: keys(:)
        integer :: i
        type(fallible_json_value_t) :: maybe_value
        type(json_object_t) :: object
        type(json_element_t), allocatable :: values(:)

        object = json_object_t( &
                [ json_member_unsafe("first", json_string_unsafe("hello")) &
                , json_member_unsafe("second", json_string_unsafe("world")) &
                , json_member_unsafe("third", json_string_unsafe("goodbye")) &
                ])
        keys = object%get_keys()
        values = object%get_values()
        result_ = assert_equals(size(keys), size(values), "number of members")
        do i = 1, size(keys)
            maybe_value = object%get_element(keys(i))
            if (maybe_value%failed()) then
                errors = maybe_value%errors()
                result_ = result_.and.fail(errors%to_string())
            else
                select type (retrieved_string => maybe_value%value_())
                type is (json_string_t)
                    select type (array_string => values(i)%value_())
                    type is (json_string_t)
                        result_ = result_.and.assert_equals( &
                                array_string%get_value(), retrieved_string%get_value())
                    class default
                        result_ = result_.and.fail("didn't get a string for value " // to_string(i))
                    end select
                class default
                    result_ = result_.and.fail("didn't retrieve a string for key " // keys(i))
                end select
            end if
        end do
    end function

    function get_value_from_object_failure() result(result_)
        type(result_t) :: result_

        type(error_list_t) :: errors
        type(json_object_t) :: object
        type(fallible_json_value_t) :: retrieved

        call object%add_unsafe("first", json_string_unsafe("hello"))
        call object%add_unsafe("second", json_string_unsafe("goodbye"))

        retrieved = object%get_element("third")
        errors = retrieved%errors()

        result_ = assert_that(errors.hasType.NOT_FOUND, errors%to_string())
    end function

    function check_string_error() result(result_)
        type(result_t) :: result_

        type(error_list_t) :: errors
        type(fallible_json_string_t) :: string

        string = fallible_json_string_t("\invalid")
        errors = string%errors()

        result_ = assert_that(errors.hasType.INVALID_INPUT, errors%to_string())
    end function
end module
