---
project: JSON for Fortran
summary: A functional JSON parser for Fortran
project_website: https://gitlab.com/everythingfunctional/jsonff
project_download: https://gitlab.com/everythingfunctional/jsonff/-/releases
author: Brad Richardson
email: everythingfunctional@protonmail.com
website: https://everythingfunctional.com
twitter: https://twitter.com/everythingfunct
github: https://github.com/everythingfunctional
src_dir: ../src
display: public
         protected
         private
sort: permission-alpha
output_dir: ../public
graph: true
extra_mods: parff:https://everythingfunctional.gitlab.io/parff/
            erloff:https://everythingfunctional.gitlab.io/erloff/
            strff:https://everythingfunctional.gitlab.io/strff/
            iso_varying_string:https://everythingfunctional.gitlab.io/iso_varying_string/
            vegetables:https://everythingfunctional.gitlab.io/vegetables/
...
